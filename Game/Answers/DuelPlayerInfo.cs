﻿using System.Diagnostics.CodeAnalysis;


namespace Game.Answers;

[SuppressMessage("ReSharper", "NotAccessedPositionalProperty.Global")]
public record DuelPlayerInfo(string Name, int Health);