﻿using System.Diagnostics.CodeAnalysis;


namespace Game.Answers;

[SuppressMessage("ReSharper", "NotAccessedPositionalProperty.Global")]
public record DuelStatsInfo(DuelPlayerInfo CurrentPlayer, DuelPlayerInfo Opponent);