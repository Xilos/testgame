﻿using System.Diagnostics.CodeAnalysis;


namespace Game.Answers;

[SuppressMessage("ReSharper", "NotAccessedPositionalProperty.Global")]
public record OpenedDuelInfo(string Title, string Id);