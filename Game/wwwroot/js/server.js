﻿"use strict";

document.getElementById("duel").style.display = "none";

var builder = new signalR.HubConnectionBuilder();
var connection = builder.withUrl("/serverHub").build();

connection.on("UpdateLobbiesInfo", function (lobbies) {
    document.getElementById("lobbiesList").innerHTML = "";
    lobbies.forEach(lobby => {
        var li = document.createElement("li");
        document.getElementById("lobbiesList").appendChild(li);
        li.textContent = `${lobby.title}`;

        var btn = document.createElement("input");
        btn.setAttribute("type", "button");
        btn.setAttribute("id", `${lobby.id}`);
        btn.setAttribute("value", "Join");
        btn.addEventListener("click", function (event) {
            var id = event.target.getAttribute("id");
            connection.invoke("JoinDuel", id).catch(function (err) {
                return console.error(err.toString());
            });
            event.preventDefault();
        });

        li.appendChild(btn);
    });
});

connection.on("EnteredDuel", function () {
    document.getElementById("lobby").style.display = "none";
    document.getElementById("duel").style.display = "block";
    document.getElementById("battleTitle").style.display = "none";
});

connection.on("UpdateDuelInfo", function (duelInfo) {
    document.getElementById("battleTitle").style.display = "block";
    document.getElementById("waitingBanner").style.display = "none";

    document.getElementById("firstPlayerText").innerHTML = `${duelInfo.currentPlayer.name} (you): ${duelInfo.currentPlayer.health}`;
    document.getElementById("secondPlayerText").innerHTML = `${duelInfo.opponent.name}: ${duelInfo.opponent.health}`;
});

connection.on("EndDuel", function (status) {
    document.getElementById("battleTitle").style.display = "none";
    document.getElementById("waitingBanner").style.display = "block";
    document.getElementById("lobby").style.display = "block";
    document.getElementById("duel").style.display = "none";

    document.getElementById("firstPlayerText").innerHTML = "";
    document.getElementById("secondPlayerText").innerHTML = "";

    alert(status);
});

connection.start();

document.getElementById("createLobbyButton").addEventListener("click", function (event) {
    connection.invoke("CreateDuel", null).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});