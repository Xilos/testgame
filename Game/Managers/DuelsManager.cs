﻿using System.Collections.Concurrent;

using Game.Entities;


namespace Game.Managers;

internal static class DuelsManager
{
	private static readonly ConcurrentDictionary<string, Duel> _duels = new();

	public static void CreateDuel(User hostUser)
	{
		Player player = Player.FromUser(hostUser);
		var duel = new Duel(player);
		duel.DuelEnded += OnDuelEnded;
		_duels.GetOrAdd(hostUser.ConnectionId, duel);
	}

	public static Duel GetDuel(string duelId) => _duels[duelId];

	public static IEnumerable<Duel> GetOpennedDuels() => _duels.Values.Where(duel => duel.IsStarted == false);

	public static void AddUserToDuel(string duelId, User user)
	{
		if (_duels.TryGetValue(duelId, out Duel? duel))
		{
			Player player = Player.FromUser(user);
			duel.AddPlayer(player);
		}
	}


	private static void OnDuelEnded(object? sender, EventArgs e)
	{
		if (sender is Duel duel)
		{
			duel.DuelEnded -= OnDuelEnded;
			duel.Dispose();
			_duels.TryRemove(duel.Id, out Duel? _);
		}
	}
}