﻿using System.Collections.Concurrent;

using Game.Entities;


namespace Game.Managers;

internal static class UsersManager
{
	private static readonly ConcurrentDictionary<string, User> _users = new();

	public static void AddUser(string connectionId)
	{
		if (_users.TryGetValue(connectionId, out User? _))
			return;

		var user = new User(connectionId, GetNextPlayerIndex());
		_users.GetOrAdd(connectionId, user);
	}

	public static bool RemoveUser(string connectionId) => _users.Remove(connectionId, out User? _);

	public static User GetUser(string connectionId) => _users[connectionId];

	private static int GetNextPlayerIndex()
	{
		int[] usersIndices = _users.Values.Select(user => user.Index).ToArray();
		int i = 1;
		while (usersIndices.Contains(i))
			i++;

		return i;
	}
}