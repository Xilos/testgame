﻿using Game.Answers;
using Game.Entities;
using Game.Listeners;
using Game.Managers;

using Microsoft.AspNetCore.SignalR;


namespace Game.Hubs;

public class ServerHub : Hub
{
	public async Task CreateDuel(object _)
	{
		User user = UsersManager.GetUser(Context.ConnectionId);
		DuelsManager.CreateDuel(user);

		await UpdateLobbiesInfoFor(Clients.All);
		await OnCallerEnteredDuel();
	}

	public async Task JoinDuel(string duelId)
	{
		User user = UsersManager.GetUser(Context.ConnectionId);
		DuelsManager.AddUserToDuel(duelId, user);

		Duel duel = DuelsManager.GetDuel(duelId);
		DuelListener.Instance.AddToListen(duel);

		await OnCallerEnteredDuel();
		duel.Start();
		await UpdateLobbiesInfoFor(Clients.All);
	}

	public override async Task OnConnectedAsync()
	{
		UsersManager.AddUser(Context.ConnectionId);
		await UpdateLobbiesInfoFor(Clients.Caller);
	}

	public override async Task OnDisconnectedAsync(Exception? exception)
	{
		UsersManager.RemoveUser(Context.ConnectionId);
		await Task.CompletedTask;
	}


	private async Task OnCallerEnteredDuel()
	{
		await Clients.Caller.SendCoreAsync("EnteredDuel", Array.Empty<object>());
	}

	private static async Task UpdateLobbiesInfoFor(IClientProxy client)
	{
		IEnumerable<Duel> opennedDuels = DuelsManager.GetOpennedDuels();
		await client.SendCoreAsync("UpdateLobbiesInfo", new object?[] { opennedDuels.Select(ToOpenedDuelInfo) });
	}

	private static OpenedDuelInfo ToOpenedDuelInfo(Duel duel) => new OpenedDuelInfo(duel.Title, duel.Id);
}