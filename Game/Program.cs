using Game.Hubs;
using Game.Listeners;


WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();

builder.Services.AddSignalR();
builder.Services.AddSingleton<DuelListener>();

WebApplication app = builder.Build();

app.MapHub<ServerHub>("/serverHub");

app.Services.GetRequiredService<DuelListener>();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
	app.UseExceptionHandler("/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();

app.Run();
