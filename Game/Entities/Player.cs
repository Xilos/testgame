﻿namespace Game.Entities;

internal class Player
{
	private const int DefaultHealth = 10;

	private Player(User user)
	{
		User = user;
	}

	public User User { get; }

	public int Health { get; set; } = DefaultHealth;

	public bool IsAlive => Health > 0;


	public static Player FromUser(User user) => new(user);
}