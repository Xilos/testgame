﻿namespace Game.Entities;

internal record User(string ConnectionId, int Index)
{
	public string Name => $"Player {Index}";
}