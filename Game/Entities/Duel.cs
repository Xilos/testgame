﻿namespace Game.Entities;

internal class Duel : IDisposable
{
	private Timer? _timer;
	private readonly Random _rnd = new();


	public Duel(Player host)
	{
		Host = host;
	}
	

	public Player Host { get; }

	public Player? Player2 { get; private set; }

	public string Title => Host.User.Name;

	public string Id => Host.User.ConnectionId;

	public bool IsStarted { get; private set; }


	public event EventHandler? DuelStatsUpdated;

	public event EventHandler? DuelEnded;


	public void AddPlayer(Player player)
	{
		Player2 ??= player;
	}

	public void Start()
	{
		_timer = new Timer(DuelMove, null, 0, 1000);
		IsStarted = true;
		DuelStatsUpdated?.Invoke(this, EventArgs.Empty);
	}

	private void DuelMove(object? state)
	{
		int user1Health = Host.Health;
		int user2Health = Player2.Health;

		user1Health -= GetDamage();
		user2Health -= GetDamage();

		Host.Health = user1Health;
		bool hasAtLeastOnePlayerAlive = user1Health > 0 || user2Health > 0;
		if (hasAtLeastOnePlayerAlive)
		{
			Player2.Health = user2Health;
		}

		DuelStatsUpdated?.Invoke(this, EventArgs.Empty);

		bool hasWinner = user1Health <= 0 || user2Health <= 0;
		if (hasWinner)
			DuelEnded?.Invoke(this, EventArgs.Empty);

	}

	private int GetDamage() => _rnd.Next(0, 2);

	public void Dispose()
	{
		_timer?.Dispose();
	}
}