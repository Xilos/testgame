﻿using Game.Answers;
using Game.Entities;
using Game.Hubs;

using Microsoft.AspNetCore.SignalR;


namespace Game.Listeners
{
	internal class DuelListener
    {
	    private readonly IHubContext<ServerHub> _hubContext;

	    public DuelListener(IHubContext<ServerHub> hubContext)
	    {
		    _hubContext = hubContext;
		    Instance = this;
	    }


		public static DuelListener Instance { get; private set; }


		public void AddToListen(Duel duel)
		{
			duel.DuelStatsUpdated += OnDuelStatsUpdated;
			duel.DuelEnded += OnDuelEnded;
		}


		private async void OnDuelStatsUpdated(object? sender, EventArgs eventArgs)
		{
			var duel = sender as Duel;
			Player player1 = duel.Host;
			Player player2 = duel.Player2;

			await _hubContext.Clients.Client(player1.User.ConnectionId).SendCoreAsync("UpdateDuelInfo", new object?[] { CreateDuelStatsInfoFor(player1, player2) });
			await _hubContext.Clients.Client(player2.User.ConnectionId).SendCoreAsync("UpdateDuelInfo", new object?[] { CreateDuelStatsInfoFor(player2, player1) });
		}

		private static DuelStatsInfo CreateDuelStatsInfoFor(Player player1, Player player2)
		{
			return new DuelStatsInfo(ToDuelPlayerInfo(player1), ToDuelPlayerInfo(player2));
		}

		private static DuelPlayerInfo ToDuelPlayerInfo(Player player)
		{
			return new DuelPlayerInfo(player.User.Name, player.Health);
		}

		private async void OnDuelEnded(object? sender, EventArgs e)
		{
			var duel = sender as Duel;
			Player player1 = duel.Host;
			Player player2 = duel.Player2;

			await _hubContext.Clients.Client(player1.User.ConnectionId).SendCoreAsync("EndDuel", new object?[] { player1.IsAlive ? "You won" : "You loose" });
			await _hubContext.Clients.Client(player2.User.ConnectionId).SendCoreAsync("EndDuel", new object?[] { player2.IsAlive ? "You won" : "You loose" });

			duel.DuelStatsUpdated -= OnDuelStatsUpdated;
			duel.DuelEnded -= OnDuelEnded;
		}
	}
}
